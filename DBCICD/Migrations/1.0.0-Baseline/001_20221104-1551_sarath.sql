﻿-- <Migration ID="bc3bbd43-2faf-4b7c-9fb1-79ef573ddf7b" />
GO

DECLARE @associate bit
SELECT @associate = CASE SERVERPROPERTY('EngineEdition') WHEN 5 THEN 1 ELSE 0 END
IF @associate = 0 EXEC sp_executesql N'SELECT @count = COUNT(*) FROM master.dbo.syslogins WHERE loginname = N''sarath''', N'@count bit OUT', @associate OUT
IF @associate = 1
BEGIN
    PRINT N'Creating user [sarath] and mapping to the login [sarath]'
    CREATE USER [sarath] FOR LOGIN [sarath]
END
ELSE
BEGIN
    PRINT N'Creating user [sarath] without login'
    CREATE USER [sarath] WITHOUT LOGIN
END
GO
